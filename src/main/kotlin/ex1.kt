import kotlinx.coroutines.*

suspend fun main() {
    val coroutineList = listOf<Coroutine>(Coroutine("Coroutine 1"), Coroutine("Coroutine 2"), Coroutine("Coroutine 3"), Coroutine("Coroutine 4"), Coroutine("Coroutine 5"))
    var order = 0
    runBlocking {
        coroutineList.forEach { cor ->
            launch { cor.run()
            order++
            print("$order to finish.\n")}
        }
    }
}

class Coroutine(private val name: String){
    private val randomTime = listOf<Long>(1000, 2000, 3000, 4000, 5000)

    suspend fun run(){
        for (lap in 1..5){
            delay(randomTime.random())
            when (lap){
                5 -> {
                    print("$name was the number ")
                }
            }
        }

    }
}

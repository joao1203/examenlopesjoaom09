import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

suspend fun main() {
    val coroutineList = listOf<CoroutineEx2>(CoroutineEx2("Coroutine 1"), CoroutineEx2("Coroutine 2"), CoroutineEx2("Coroutine 3"), CoroutineEx2("Coroutine 4"), CoroutineEx2("Coroutine 5"))
    var order = 0
    var totalTime = 0L

    runBlocking {
        coroutineList.forEach { cor ->
            launch { cor.run() }
        }
    }


}

class CoroutineEx2(private val name: String){
    val randomTime = listOf<Long>(1000, 2000, 3000, 4000, 5000)
    var totalTime = 0L
    var laps = 0

    suspend fun run(){
        while (totalTime < 15000){
            val time = measureTimeMillis {
                delay(randomTime.random())
                laps++
                delay(100)
            }
            totalTime += time
        }
        println("$name did $laps laps!")
    }
}
